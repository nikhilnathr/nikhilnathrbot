<?php
require_once('../../config/keys.php');
require_once('../../config/database.php');

class KTU {

  public $TLD = "https://www.ktu.edu.in";
  public $data = array();
  public $log_data = array();
  private $log_path = "prev.txt";

  function __construct() {
    $all_contents = file_get_contents($this->TLD ."/eu/res/viewResults.htm");
    $start = explode("<tbody>", explode("</tbody>", explode('<table id="listingTable" class=" table table-striped table-bordered table-hover" width="100%">', $all_contents)[1])[0])[1];

    // push the data between each upcoming td tags to an array
    $elems = explode("<td>", $start);
    array_splice($elems, 0, 1);

    for ($i = 0; $i < sizeof($elems); $i++) {
      $elems[$i] = trim(substr($elems[$i], 0, strpos($elems[$i], "</td>")));

      $linkPart = substr($elems[$i], strpos($elems[$i], 'href="') + 6, strpos($elems[$i], '">') - strpos($elems[$i], 'href="') - 6);
      $dataPart = substr($elems[$i], strpos($elems[$i], '">') + 2, strpos($elems[$i], '</a>') - strpos($elems[$i], '">') - 2);

      $this->data[$i]['link'] = $this->TLD.$linkPart;
      $this->data[$i]['data'] = trim($dataPart);

      $this->log_data = json_decode(file_get_contents($this->log_path));
    }
  }

  function json_object() {
    return json_encode($this->data);
  }

  function update_last() {
    return file_put_contents($this->log_path, $this->json_object());
  }

  function get_new_results() {
    $new_news = array();
    foreach ($this->data as $data) {
      $present = false;
      if ($this->log_data != "") {
        foreach ($this->log_data as $log_data) {
          if ($log_data->data == $data['data']) {
            $present = true;
            break;
          }
        }
      }
      if (!$present) {
        array_push($new_news, $data);
      }
    }
    return (sizeof($new_news) < 1)? false: $new_news;
  }
}

$myKTU = new KTU();
$new_news = $myKTU->get_new_results();

if ($new_news != false) {
  // send the new news to all the subscribers and update log

  // select all the subscribers to ktu channel
  $users = array();
  $query = "SELECT userid FROM users WHERE ktu=1";
  $stmt = $conn->prepare($query);
  $stmt->execute();
  $usersRes = $stmt->get_result();
  while ($user = $usersRes->fetch_assoc())  {
    array_push($users, array("id" => $user['userid']));
  }

  // update log
  $myKTU->update_last();

  // send the new news to all subscribers
  foreach ($new_news as $news) {
    foreach($users as $user) {
      $message = $news['data'];
      sendMessage($user['id'], $news, $APIKEY);
    }
  }

  // log the data sent
  echo json_encode($new_news);
}

// time to display in the log
echo date('r', time()) . "\n\n";


function sendMessage($id, $message, $APIKEY) {

  $ogLink = "https://api.telegram.org/bot$APIKEY/sendMessage";
  $query_string = http_build_query(array(
    "chat_id" => $id,
    "text" =>  "<a href='". $message['link'] . "'>". $message['data'] ."</a>",
    "parse_mode" => "HTML"
  ));

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL,"$ogLink");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $server_output = curl_exec ($ch);

  curl_close ($ch);
}

?>
