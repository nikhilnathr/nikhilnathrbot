<?php
require_once('./rss_php.php');
require_once('../config/keys.php');
require_once('../config/database.php');

$nowTime = time();    // time to update at the last
$data_sent = false;

// get timestamp of last check from db
$query = "SELECT lastcheck FROM lastchecks WHERE channel='yts'";
$stmt = $conn->prepare($query);
$stmt->execute();
$dbLastCheck = $stmt->get_result()->fetch_assoc()['lastcheck'];

$rss = new rss_php;
$rss->load($YTSLINK);
$items = $rss->getItems(); #returns all rss items
$temp = $rss->getItems(true); #returns all rss items with attribute
for ( $i = 0; $i < sizeof($items); $i++) {
  $items[$i]['enclosure'] = $temp[$i]['enclosure']['properties'];
}
$latestItemTime = strtotime($items[0]['pubDate']);  // get timestamp of the latest entry from RSS

// get a list of users subscribed to yts channel
$users = array();
$query = "SELECT userid, ytsMinRating FROM users WHERE yts=1";
$stmt = $conn->prepare($query);
$stmt->execute();
$usersRes = $stmt->get_result();
while ($user = $usersRes->fetch_assoc())  {
  array_push($users, array("id" => $user['userid'], "min_rating" => $user['ytsMinRating']));
}

foreach ($items as $item) {
  $item['pubDate'] = strtotime($item['pubDate']);

  if ($item['pubDate'] > $dbLastCheck) {

    // get necessary data about film
    $descriptionsOld = explode("<br />", $item['description']);
    $description = "";

    // get link of image
    $temp = explode('src="', $descriptionsOld[0]);
    $picLink = substr($temp[1], 0, strpos($temp[1], '"'));
    for ($i = 1; $i < (sizeof($descriptionsOld) - 1); $i++) {
      $description .= $descriptionsOld[$i]."\n";
    }
    // send info about these movies to userslist
    foreach ($users as $user) {
      if ( strpos($descriptionsOld[1], "IMDB Rating: ") !== false ) {
        $rating = explode("/", explode(" ", $descriptionsOld[1])[2])[0];
        if (intval($rating) < $user['min_rating']) {
          continue;
        }
      }

      $ogLink = "https://api.telegram.org/bot$APIKEY/sendPhoto";
      $query_string = http_build_query(array(
        "chat_id" => $user['id'],
        "photo" => $picLink,
        "caption" => $item['title'] ."\n\n<a href='". $item['link'] . "'>YTS website</a>\n\n<a href='". $item['enclosure']['url'] . "'>Torrent link</a>\n\n". $description,
        "parse_mode" => "HTML",
        "disable_web_page_preview" => "true"
      ));

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL,"$ogLink");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $server_output = curl_exec ($ch);
      if ( !$data_sent && $server_output != '') {
        $data_sent = true;
      }
      var_dump($server_output);

      curl_close ($ch);
    }
  }
}

if ($data_sent) {
  // UPDATE LASTCHECK
  $query = "UPDATE lastchecks SET lastcheck=? WHERE channel='yts'";
  $stmt = $conn->prepare($query);
  $stmt->bind_param("s", $nowTime);
  $stmt->execute();
}
var_dump($data_sent);
echo date('r', $nowTime) ."\n\n";
