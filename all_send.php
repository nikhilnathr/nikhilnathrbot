<?php
  // VALIDATION USING A CUSTOM KEY
  require_once("./config/keys.php");
  require_once("./config/database.php");

  //$datas = file_get_contents("php://input")
  if (isset($_POST['qwerty']) && isset($_POST['msg']) && $_SERVER['REQUEST_METHOD'] === "POST") {
    $qwerty = trim( stripslashes( htmlspecialchars( $_POST['qwerty'] )));
    $message = $_POST['msg'];

    $i = 0;   // count number of sends
    if ( $qwerty === $CUSTOMKEY ) {
      // validation success get all subscribers and send message
      $users = array();
      $query = "SELECT userid FROM users";
      $stmt = $conn->prepare($query);
      $stmt->execute();
      $usersRes = $stmt->get_result();
      while ($user = $usersRes->fetch_assoc())  {

        $ogLink = "https://api.telegram.org/bot$APIKEY/sendMessage";
        $query_string = http_build_query(array(
          "chat_id" => $user['userid'],
          "text" => $message
        ));

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"$ogLink");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        $i++;
      }

      echo json_encode(array("status"=>"ok","number_of_messages_send"=>$i));
      exit;
    }

  }

  echo json_encode(array("status"=>"failed"));
?>
