<?php
require_once("./config/keys.php");
require_once("./config/database.php");

$datas = file_get_contents("php://input").PHP_EOL.PHP_EOL;     // get post request
$data = getLastUser(json_decode($datas));                      // get last message object
$help_text = "Please choose from the available command list\n/sub - Subscribe to available feeds\n/setrating - Set minimum rating for you to recieve notifications in available feeds\n/help - Display a list of commands";   // default help text

 // $userid = $data->message->from->id;
 // $fname = $data->message->from->first_name;
 // $lname = $data->message->from->last_name;
 // $username = $data->message->from->username;
 // $recievedMessage = $data->message->text;

 if ($_SERVER['REQUEST_METHOD'] !== "POST") {
  echo '{"status":"failed","message":"invalid request method"}';
  exit;
 }

// CHECK IF THE USER TYPED A BOT COMMAND
if (isset($data->message->entities) && $data->message->entities[0]->type == "bot_command") {

  $id = $data->message->chat->id;
  $fname = $data->message->from->first_name;
  $lname = $data->message->from->last_name;
  $username = $data->message->from->username;
  $recievedMessage = $data->message->text;

  // CHECK FIRST COMMAND WHICH WAS GIVEN
  $command = explode(" ", $data->message->text)[0];

  if ($command === "/start" || $command === "/start@$BOTNAME") {

    $message = "Hi, $fname\n". $help_text;
    sendMessage($id, $message, $APIKEY);

  } else if ($command === "/sub" || $command === "/sub@$BOTNAME") {

    $replyMarkup = '{"inline_keyboard":[[{"text":"YTS","callback_data":"subscribe-yts"}]]}';
    $message = "Select a channel for subscribing to";

    sendMessageData($id, $message, $replyMarkup, $APIKEY);

  } else if ($command === "/help" || $command === "/help@$BOTNAME") {

    $message = $help_text;
    sendMessage($id, $message, $APIKEY);

  } else if ($command === "/setrating" || $command === "/setrating@$BOTNAME") {

    $replyMarkup = '{"inline_keyboard":[[{"text":"0","callback_data":"rating-yts-0"},{"text":"1","callback_data":"rating-yts-1"},{"text":"2","callback_data":"rating-yts-2"},{"text":"3","callback_data":"rating-yts-3"},{"text":"4","callback_data":"rating-yts-4"}],
                                         [{"text":"5","callback_data":"rating-yts-5"},{"text":"6","callback_data":"rating-yts-6"},{"text":"7","callback_data":"rating-yts-7"},{"text":"8","callback_data":"rating-yts-8"},{"text":"9","callback_data":"rating-yts-9"}]]}';
    $message = "Select minimum rating for YTS films you recieve";

    sendMessageData($id, $message, $replyMarkup, $APIKEY);

  } else {
    $message = "That command doesn't exist 😑\n". $help_text;
    sendMessage($id, $message, $APIKEY);
  }
}


// CHECK IF CALLBACK QUERY
else if (isset($data->callback_query)) {
  // check which action was triggered
  $action = $data->callback_query->data;
  $callback_id = $data->callback_query->id;
  $id = $data->callback_query->from->id;
  $fname = $data->callback_query->from->first_name;
  $lname = $data->callback_query->from->last_name;
  $username = $data->callback_query->from->username;

  $original_message_id = $data->callback_query->message->message_id;
  $original_chat_id = $data->callback_query->message->chat->id;
  $isPrivate = ($data->callback_query->message->chat->type === "private")? true : false;

  // if it is subscribe-yts action
  if ($action === "subscribe-yts") {

    if ( saveSubscriber($id, $username, $fname, $lname, $conn) ) {

      if( $isPrivate ){
        $message = "Thank you for subscribing to YTS feed 😄";
      } else {
        $message = $fname .", thank you for subscribing to YTS feed 😄";
      }
      editMessageText($original_chat_id, $original_message_id, $message, $APIKEY);

    } else {
      if( $isPrivate ){
        $message = "You are already subsribed to YTS feed 😃";
      } else {
        $message = $fname .", you are already subscribed to YTS feed 😄";
      }
      editMessageText($original_chat_id, $original_message_id, $message, $APIKEY);
    }
    answerCallbackQuery($callback_id, "", $APIKEY);


  }
  // if it is rating-yts-$ action
  else if (substr($action, 0, 11) === "rating-yts-" && strlen($action) === 12 && is_numeric($action[strlen($action)-1])) {

    $given_rating = $action[strlen($action)-1];
    if ( saveMinRating($id, $given_rating, $conn) ) {

      if( $isPrivate ) {
        $message = "You will only recieve notification for films having IMDB rating greater than $given_rating";
      } else {
        $message = $fname .", you will only recieve notification for films having IMDB rating greater than $given_rating";
      }
      editMessageText($original_chat_id, $original_message_id, $message, $APIKEY);

    } else {
      if( $isPrivate ){
        $message = "First subscribe to the channel to set a rating. Subscribe using the /sub command";
      } else {
        $message = $fname .", first subscribe to the channel to set a rating. Subscribe using the /sub command";
      }
      editMessageText($original_chat_id, $original_message_id, $message, $APIKEY);
    }

    answerCallbackQuery($callback_id, "", $APIKEY);


  } else  {

    $message = "This is bad 😠";
    $id = $data->callback_query->from->id;
    sendMessage($id, $message, $APIKEY);

  }
}

else {

  $message = $data->message->from->first_name." mandan aan 😛";
  $id = $data->message->chat->id;
  sendMessage($id, $message, $APIKEY);

}



logData($datas);


function logData($data) {
  $file = 'logs.txt';

  if (file_put_contents($file, $data, FILE_APPEND | LOCK_EX) === false) {
    echo '{"status":"failed","message":"Error in writing to log file"}';
  } else {
    echo '{"status":"ok"}';
  }
}

// SAVE THE MINIMUM RATING TO DATABASE
function saveMinRating($id, $given_rating, $conn) {
  $given_rating = intval($given_rating);
  $query = "SELECT count(id) AS num FROM users WHERE userid=? AND yts=1";
  $stmt = $conn->prepare($query);
  $stmt->bind_param("i", $id);
  $stmt->execute();
  $alreadySubscribed = $stmt->get_result()->fetch_assoc()['num'];
  if ($alreadySubscribed > 0) {

    $query = "UPDATE users SET ytsMinRating=? WHERE userid=?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param("ii", $given_rating, $id);
    $stmt->execute();

    return true;
  }
  return false;
}
// SAVE THE DATA OF SUBSCRIBER TO DATABASE
function saveSubscriber($id, $username, $fname, $lname, $conn) {
  $query = "SELECT count(id) AS num FROM users WHERE userid=? AND yts=1";
  $stmt = $conn->prepare($query);
  $stmt->bind_param("i", $id);
  $stmt->execute();
  $alreadySubscribed = $stmt->get_result()->fetch_assoc()['num'];
  if ($alreadySubscribed < 1) {

    $query = "SELECT count(id) AS num FROM users WHERE userid=?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $alreadyExists = $stmt->get_result()->fetch_assoc()['num'];

    if ($alreadyExists > 0) {
      $query = "UPDATE users SET yts=1 WHERE userid=?)";
      $stmt = $conn->prepare($query);
      $stmt->bind_param("i", $id);
      $stmt->execute();
    } else {
      $query = "INSERT INTO users (userid, username, fname, lname, datemade, yts) VALUES(?, ?, ?, ?, now(), 1)";
      $stmt = $conn->prepare($query);
      $stmt->bind_param("isss", $id, $username, $fname, $lname);
      $stmt->execute();
    }

    return true;
  }
  return false;
}

function editMessageText($chat_id, $message_id, $text, $APIKEY) {
  $ogLink = "https://api.telegram.org/bot$APIKEY/editMessageText";
  $http_query = http_build_query(array(
    "chat_id" => $chat_id,
    "message_id" => $message_id,
    "text" => $text
  ));

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL,"$ogLink");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $http_query);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

  // receive server response ...
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $server_output = curl_exec ($ch);
  curl_close ($ch);
}

function answerCallbackQuery($id, $message, $APIKEY) {

  $ogLink = "https://api.telegram.org/bot$APIKEY/answerCallbackQuery";
  $http_query = http_build_query(array(
    "callback_query_id" => $id,
    "text" => $message,
  ));

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL,"$ogLink");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $http_query);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

  // receive server response ...
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $server_output = curl_exec ($ch);
  curl_close ($ch);
}

function sendMessageData($id, $message, $reply_markup, $APIKEY) {

  $ogLink = "https://api.telegram.org/bot$APIKEY/sendMessage";
  $http_query = http_build_query(array(
    "chat_id" => $id,
    "text" => $message
  ));
  $http_query .= "&reply_markup=$reply_markup";

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL,"$ogLink");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $http_query);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

  // receive server response ...
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $server_output = curl_exec ($ch);
  curl_close ($ch);
}
function sendMessage($id, $message, $APIKEY) {

  $ogLink = "https://api.telegram.org/bot$APIKEY/sendMessage";
  $query_string = http_build_query(array(
    "chat_id" => $id,
    "text" => $message
  ));

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL,"$ogLink");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $server_output = curl_exec ($ch);

  curl_close ($ch);
}

function getLastUser ($datas) {
  if (is_array($datas)) {
    $data = $datas[sizeof($datas)-1];
  } else {
    $data = $datas;
  }
  return $data;
}
